<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use Carbon\Carbon; //date package ng laravel just like moment.js ng PHP

class EmpSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        for ($i=0; $i < 20; $i++) { 
            DB::table('employees')->insert([
                [
                    'name' => $faker->name,
                    'created_at' => Carbon::now(),
                ],
            ]);
        }
    }
}
