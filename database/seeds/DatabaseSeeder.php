<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // Calls the class
        $this->call([
            UserSeeder::class,
            // EmpSeeder::class
        ]);
    }
}
