<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use Carbon\Carbon;
class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Seeds 20 times to both tables
        $faker = Faker::create();
        for ($i=0; $i < 20; $i++) { 

            $fake = $faker->name;
            DB::table('users')->insert([
                [
                    'name' => $fake,
                    'email' => $faker->unique()->email,
                    'password' =>Hash::make('admin123') //common password
                ],
            ]);

            DB::table('employees')->insert([
                [
                    'name' => $fake,
                    'created_at' => Carbon::now(),
                ],
            ]);
        }
    }
}
