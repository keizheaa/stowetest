<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Traits\AddTrait;
use Carbon\Carbon;

use DB;

class User extends Authenticatable
{
    use Notifiable;
    use AddTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];


    // public static function addNameToTestTable($data)
    // {

    //     // return "Hello World!";

    //     DB::table('add_name_to_test_tables')->insert([
    //         'user_employee_name' => $data, //kung gusto mo kunin yung name yung Auth or Session na lang
    //         'created_at' => Carbon::now(),
    //         'updated_at' => Carbon::now(),
    //     ]);
    // }

    public static function testing()
    {
        return "Hello World!";
    }
}
