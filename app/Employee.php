<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Traits\AddTrait;

class Employee extends Model
{

    use AddTrait;
    //
    // Table Name
    protected $table = 'employees';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 
    ];

    // Primary Key
    public $primaryKey = 'id';

    // Timestamps
    public $timestamps = true;

    // public function user(){
    //     return $this->belongsTo('App\User');
    // }
}
