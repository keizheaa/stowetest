<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Traits\AddTrait;

class AddNameToTestTable extends Model
{
    use AddTrait;
    //
    protected $table = 'add_name_to_test_tables';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_employee_name', 
    ];

    // Primary Key
    public $primaryKey = 'id';

    // Timestamps
    public $timestamps = true;
}
