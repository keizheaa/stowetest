<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\AddNameToTestTable;
use Auth;

class HomeController extends Controller
{
    /**
     * 
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        //
        $user_employee_name = AddNameToTestTable::orderBy('created_at','desc')->get();
        return view('home', compact('user_employee_name'));
    }

    public function saveEmployee()
    {
        // Calling of Traits
        // return(Auth::user()->name);
        
        //OOP Static method sa PHP ganito mo tawagin
        AddNameToTestTable::addNameToTestTable(Auth::user()->name);
         return redirect('home');

        //OOP method sa PHP ganito mo tawagin
        // $user = new User;
        // return $user->addNameToTestTable();

    }
}
