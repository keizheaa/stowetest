<?php
namespace App\Traits;

use Auth;
use App\AddNameToTestTable;
use DB;
use Carbon\Carbon;

trait AddTrait {

    public static function addNameToTestTable($data)
    {

        DB::table('add_name_to_test_tables')->insert([
            'user_employee_name' => $data, 
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
    }
}