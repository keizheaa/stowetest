@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard Main</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    You are logged in!

                    <div class="form-group row mb-0">
                        <div class="col-md-8 offset-md-4">
                            <form action="{{ route('saveEmployee') }}" method="post">
                                {{ csrf_field() }}
                                <button type="submit" class="btn btn-primary">
                                    TEST
                                </button>
                            </form>
                        </div>
                    </div><br>

                    @if(count($user_employee_name) > 0)
                        <div class="form-group row mb-0">
                            <div class="col-md-12">
                                <table class="table table-striped">
                                    <tr>
                                        <th>Name</th>
                                        <th>Created At</th>
                                    </tr>
                                    <?php $test = \App\User::testing()?>
                                    {{-- {{ $test }} --}}
                                    @foreach ($user_employee_name as $item)
                                    <tr>
                                            <td>{{$item->user_employee_name}}</td>
                                            <td>{{$item->created_at}}</td>
                                        </tr>
                                    @endforeach
                                    
                                </table>
                            </div>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
